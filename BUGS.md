#1
BUG
use strace in htop on bin/httpd-min and we get this error:
loop exited
error: loop_run (-1):
Unknown error -1
(probably a signal interrupting the call to epoll_pwait?)

#2
BUG
SIGWNCH causes loop to exit with the signal interrupting the event loop
(probably a signal interrupting the call to epoll_pwait?)
