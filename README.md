iterate list if descriptors
lookup descriptor based on index/key (fd)
allocate/create a descriptor and add to list
free/delete a descriptor and remove it from list
free/delete list of descriptors

valgrind --track-origins=yes -v --leak-check=full bin/httpd-min 


ltrace -cfST bin/httpd

taskset 0x00000001 bin/httpd 8
wrk -c 128 -t 8 -d 20 http://127.0.0.1:3000/

Requests/sec: 18.476m
cpu: 120%
latency: avg 2.35ms max 54.86ms stdev 90.06%
Transfer/sec: 646 MB
intel core i5-8250U @ 1.6 GHz
cache size: 6144k

strace -TCf -e trace=writev,read,epoll_pwait,accept,fcntl,mprotect,epoll_ctl,setsockopt,close bin/httpd 2


done:
tcp client
tcp server
http client
http server
tty stdin
tty stdout
timer
signals
threads
tls client

todo:
udp
unix sockets
pipes
zlib
crypto - hmac/hash


20k outbuffers per connection
512 buffer per connection
64k read buffer per connection (can be shared?)
40 bytes payload
512 * 40 = 20480 bytes
85k per connection

10 = 850K
100 = 8.5M
1000 = 85M
10000 = 850M
100000 = 8.5G
1000000 = 85G
