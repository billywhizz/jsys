## compiler to use
CC             = gcc

## compile and link flags
CFLAGS         = -DTRACE=0 -D_DEFAULT_SOURCE -Wimplicit-fallthrough -Wall -pedantic -std=c11 -flto -g -O3 -march=native -mtune=native -I./include
LDADD          = -static -m64 -flto

## targets
.PHONY: help clean

help: ## display this help message
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

httpd-valgrind: examples/httpd-min.o src/picohttpparser.o ## debug non static build for valgrind
	$(CC) -o bin/$@ $^ -DTRACE=0 -D_DEFAULT_SOURCE -Wimplicit-fallthrough -Wall -pedantic -std=c11 -flto -g -march=native -mtune=native -I./src -m64

valgrind:
	./valgrind.sh

httpd-min: examples/httpd-min.o src/picohttpparser.o ## http demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD)

httpd: examples/httpd.o src/picohttpparser.o ## another http demo with threads
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD) -lpthread

tcp: examples/tcp.o ## tcp demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD)

tty: examples/tty.o ## tty demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD)

cat: examples/cat.o ## unix cat demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD)

simple: examples/simple.o ## simple tcp demo with no jsys
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD)

stress: examples/stress.o ## stress test with threads
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD) -lpthread

wrk: examples/wrk.o src/picohttpparser.o ## wrk http benchmarking tool clone
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD) -lpthread

tls: examples/tls.o ## tls http client demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD) /usr/lib/x86_64-linux-gnu/libssl.a /usr/lib/x86_64-linux-gnu/libcrypto.a -lpthread -ldl

client: examples/client.o src/picohttpparser.o ## http client demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD) -lpthread

threads: examples/threads.o ## threads demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD) -lpthread

timer: examples/timer.o ## timer demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD)

signal: examples/signal.o ## signal demo
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD)

multi: examples/multi.o ## multiple pieces of functionality
	$(CC) -o bin/$@ $^ $(CFLAGS) $(LDADD)

clean: ## clean the generated artifacts
	rm -f bin/httpd examples/httpd.o \
		bin/httpd-min examples/httpd-min.o \
		bin/httpd-valgrind \
		bin/tcp examples/tcp.o \
		bin/tty examples/tty.o \
		bin/simple examples/simple.o \
		bin/cat examples/cat.o \
		bin/stress examples/stress.o \
		bin/wrk examples/wrk.o \
		bin/tls examples/tls.o \
		bin/client examples/client.o \
		bin/threads examples/threads.o \
		bin/timer examples/timer.o \
		bin/signal examples/signal.o \
		bin/multi examples/multi.o \
		src/picohttpparser.o

.DEFAULT_GOAL := help
