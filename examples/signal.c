#include "jsys.h"

int counter = 0;
#define NSIG 64

int on_signal(jsys_descriptor* signal) {
	counter++;
	struct signalfd_siginfo info;
	ssize_t r = read(signal->fd, &info, sizeof info);
	if (r != sizeof info) return -1;
	fprintf(stderr, "on_signal (%i) %s (%u)\n", counter, strsignal((int)info.ssi_signo), info.ssi_signo);
/*
	fprintf(stderr, "  signo   = %u\n", info.ssi_signo);
	fprintf(stderr, "  errno   = %i\n", info.ssi_errno);
	fprintf(stderr, "  code    = %i\n", info.ssi_code);
	fprintf(stderr, "  pid     = %u\n", info.ssi_pid);
	fprintf(stderr, "  uid     = %u\n", info.ssi_uid);
	fprintf(stderr, "  fd      = %i\n", info.ssi_fd);
	fprintf(stderr, "  tid     = %u\n", info.ssi_tid);
	fprintf(stderr, "  band    = %u\n", info.ssi_band);
	fprintf(stderr, "  overrun = %u\n", info.ssi_overrun);
	fprintf(stderr, "  trapno  = %u\n", info.ssi_trapno);
	fprintf(stderr, "  status  = %i\n", info.ssi_status);
	fprintf(stderr, "  int     = %i\n", info.ssi_int);
	fprintf(stderr, "  ptr     = %lu\n", info.ssi_ptr);
	fprintf(stderr, "  utime   = %lu\n", info.ssi_utime);
	fprintf(stderr, "  stime   = %lu\n", info.ssi_stime);
	fprintf(stderr, "  addr    = %lu\n", info.ssi_addr);
*/
  return 0;
}

void resetSignals() {
  // resets all signals to defaults and ignores SIGPIPE and SIGXFSZ
  struct sigaction act;
  memset(&act, 0, sizeof(act));
  for (unsigned nr = 1; nr < 32; nr += 1) {
    if (nr == SIGKILL || nr == SIGSTOP)
      continue;
    act.sa_handler = (nr == SIGPIPE || nr == SIGXFSZ) ? SIG_IGN : SIG_DFL;
    int r = sigaction(nr, &act, NULL);
    if (r != 0) {
      fprintf(stderr, "failed resetting signal\n");
      exit(1);
    }
  }
}

void printSigset(FILE *of, const char *prefix, const sigset_t *sigset) {
    int sig, cnt;
    cnt = 0;
    for (sig = 1; sig < NSIG; sig++) {
        if (sigismember(sigset, sig)) {
            cnt++;
            fprintf(of, "%s%d (%s)\n", prefix, sig, strsignal(sig));
        }
    }
    if (cnt == 0)
        fprintf(of, "%s<empty signal set>\n", prefix);
}

int printSigMask(FILE *of, const char *msg) {
    sigset_t currMask;
    if (msg != NULL)
        fprintf(of, "%s", msg);
    if (sigprocmask(SIG_BLOCK, NULL, &currMask) == -1)
        return -1;
    printSigset(of, "\t\t", &currMask);
    return 0;
}


int main() {
  int r = 0;

	//printSigMask(stderr, "signals");
	resetSignals();
	//printSigMask(stderr, "signals");

	//sigset_t set;
	//printSigset(stderr, "set: ", &set);

  //sigprocmask(SIG_SETMASK, NULL, &set);
  //sigaddset(&set, SIGTERM);
  //sigprocmask(SIG_SETMASK, &set, NULL);

	//printSigset(stderr, "set: ", &set);
	//printSigMask(stderr, "signals");

  fprintf(stderr, "pid     : %i\n", getpid());
  fprintf(stderr, "SIGTERM : %i\n", SIGTERM);
  fprintf(stderr, "SIGHUP  : %i\n", SIGHUP);
  fprintf(stderr, "SIGUSR1 : %i\n", SIGUSR1);
  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
	printSigset(stderr, "loop 1: ", &loop->set);
  jsys_signal_add(loop, SIGUSR1);
	printSigset(stderr, "loop 2: ", &loop->set);
  //jsys_signal_add(loop, SIGTERM);
	printSigset(stderr, "loop 3: ", &loop->set);
  jsys_descriptor *signal = jsys_signal_watcher_create(loop);
  jsys_loop_add_flags(loop, signal, EPOLLIN);
  signal->callback = on_signal;
  r = jsys_loop_run_once(loop, 1);
	while (r >= 0) {
		usleep(1000);
		r = jsys_loop_run_once(loop, 1);
	}
  return 0;
}
