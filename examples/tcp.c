#include "jsys.h"

#define BUFSIZE 16384

static jsys_loop *loop;
int rps = 0;
int on_client_data(jsys_descriptor *client, size_t bytes);
int on_timer_event(jsys_descriptor *timer);
int on_client_connect(jsys_descriptor* client);
int on_client_end(jsys_descriptor* client);
int on_signal(jsys_descriptor* signal);

int on_client_data(jsys_descriptor *client, size_t bytes) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  rps++;
  return jsys_tcp_write(client, context->out);
}

int on_timer_event(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  jsys_loop* loop = timer->loop;
  fprintf(stderr, "%i\n", rps);
  rps = 0;
  return 0;
}

int on_client_connect(jsys_descriptor* client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  context->in = calloc(1, sizeof(struct iovec));
  context->out = calloc(1, sizeof(struct iovec));
  context->in->iov_base = calloc(1, BUFSIZE);
  context->in->iov_len = BUFSIZE;
  context->out->iov_base = "HTTP/1.1 200 OK\r\nHost: foo\r\nContent-Length: 0\r\n\r\n";
  context->out->iov_len = strlen(context->out->iov_base);
  return 0;
}

int on_client_end(jsys_descriptor* client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  free(context->in);
  free(context->out);
  free(context);
  return 0;
}

int on_signal(jsys_descriptor* signal) {
	struct signalfd_siginfo info;
	ssize_t r = read(signal->fd, &info, sizeof info);
	if (r != sizeof info) return -1;
	fprintf(stderr, "on_signal %s (%u)\n", strsignal((int)info.ssi_signo), info.ssi_signo);
	fprintf(stderr, "  signo   = %u\n", info.ssi_signo);
	fprintf(stderr, "  errno   = %i\n", info.ssi_errno);
	fprintf(stderr, "  code    = %i\n", info.ssi_code);
	fprintf(stderr, "  pid     = %u\n", info.ssi_pid);
	fprintf(stderr, "  uid     = %u\n", info.ssi_uid);
	fprintf(stderr, "  fd      = %i\n", info.ssi_fd);
	fprintf(stderr, "  tid     = %u\n", info.ssi_tid);
	fprintf(stderr, "  band    = %u\n", info.ssi_band);
	fprintf(stderr, "  overrun = %u\n", info.ssi_overrun);
	fprintf(stderr, "  trapno  = %u\n", info.ssi_trapno);
	fprintf(stderr, "  status  = %i\n", info.ssi_status);
	fprintf(stderr, "  int     = %i\n", info.ssi_int);
	fprintf(stderr, "  ptr     = %lu\n", info.ssi_ptr);
	fprintf(stderr, "  utime   = %lu\n", info.ssi_utime);
	fprintf(stderr, "  stime   = %lu\n", info.ssi_stime);
	fprintf(stderr, "  addr    = %lu\n", info.ssi_addr);
  return 0;
}

int main() {
  errno = 0;
  int r = 0;
  loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return jsys_dump_error(loop, "loop_init", 0);

  jsys_signal_add(loop, SIGTERM);
  jsys_signal_add(loop, SIGHUP);
  jsys_descriptor *signal = jsys_signal_watcher_create(loop);
  if (signal == NULL) return jsys_dump_error(loop, "signal_watcher_create", 0);
  r = jsys_loop_add_flags(loop, signal, EPOLLIN); // level triggered
  if (r == -1) return jsys_dump_error(loop, "signal_loop_add", r);
  signal->callback = on_signal;

  jsys_descriptor *timer = jsys_timer_create(loop, 3000000000, 1000000000);
  if (timer == NULL) return jsys_dump_error(loop, "timer_create", 0);
  r = jsys_loop_add(loop, timer);
  if (r == -1) return jsys_dump_error(loop, "timer_loop_add", r);
  timer->callback = on_timer_event;

  jsys_stream_settings* settings = calloc(1, sizeof(jsys_stream_settings));
  settings->on_connect = on_client_connect;
  settings->on_data = on_client_data;
  settings->on_end = on_client_end;
  jsys_descriptor *server = jsys_sock_create(loop, AF_INET, SOCK_STREAM);
  server->data = settings;

  r = jsys_tcp_bind_reuse(server, 3000, INADDR_ANY);
  if (r == -1) return jsys_dump_error(loop, "tcp_bind", r);
  r = jsys_tcp_listen(server, SOMAXCONN);
  if (r == -1) return jsys_dump_error(loop, "tcp_listen", r);
  r = jsys_loop_add(loop, server);
  if (r == -1) return jsys_dump_error(loop, "tcp_loop_add", r);
  r = jsys_loop_run(loop);
  if (r == -1) return jsys_dump_error(loop, "loop_run", r);
  return 0;
}
