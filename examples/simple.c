#include <sys/epoll.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define BUFSIZE 16384
#define EVENTS 1024
char buf[BUFSIZE];

int main (int argc, char** argv) {
  int loopfd = epoll_create1(EPOLL_CLOEXEC);
  int sockfd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
  struct sockaddr_in server_addr;
  int on = 1;
  int off = 0;
  int r = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(int));
  r = setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, &on, sizeof(int));
  server_addr.sin_family = AF_INET;
  int port = 3000;
  server_addr.sin_port = htons(port);
  server_addr.sin_addr.s_addr = INADDR_ANY;
  bzero(&(server_addr.sin_zero), 8);
  r = bind(sockfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr));
  r = listen(sockfd, SOMAXCONN);
  struct epoll_event* event = (struct epoll_event *)calloc(1, sizeof(struct epoll_event));
  event->events = EPOLLIN;
  event->data.fd = sockfd;
  r = epoll_ctl(loopfd, EPOLL_CTL_ADD, sockfd, event);
  struct epoll_event events[EVENTS];
  char* response = "HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n";
  int len = strlen(response);
  r = epoll_wait(loopfd, events, EVENTS, -1);
  while (r > 0) {
    for (int i = 0; i < r; i++) {
      int fd = events[i].data.fd;
      if (fd == sockfd) {
        int clientfd = accept(sockfd, NULL, NULL);
        int flags = fcntl(clientfd, F_GETFL, 0);
        flags |= O_NONBLOCK;
        fcntl(clientfd, F_SETFL, flags);
        struct epoll_event* client_event = (struct epoll_event *)calloc(1, sizeof(struct epoll_event));
        client_event->events = EPOLLIN;
        client_event->data.fd = clientfd;
        epoll_ctl(loopfd, EPOLL_CTL_ADD, clientfd, client_event);
      } else {
        if (events[i].events & EPOLLERR || events[i].events & EPOLLHUP) {
          close(fd);
          epoll_ctl(loopfd, EPOLL_CTL_DEL, fd, NULL);
        } else if (events[i].events & EPOLLOUT) {

        } else if (events[i].events & EPOLLIN) {
          int bytes = recv(fd, buf, BUFSIZE, MSG_NOSIGNAL);
          if (bytes == -1 && errno == EAGAIN) continue;
          if (bytes == 0) {
            close(fd);
            epoll_ctl(loopfd, EPOLL_CTL_DEL, fd, NULL);
            continue;
          }
          send(fd, response, len, MSG_NOSIGNAL);
        }
      }
    }
    r = epoll_wait(loopfd, events, EVENTS, -1);
  }
  return 0;
}