#include "jsys.h"

#define BUFSIZE 64 * 1024

static char buf[BUFSIZE];
static uint64_t total = 0;
static uint64_t gtotal = 0;

int on_tty_event(jsys_descriptor *client);
int dump_error(jsys_loop* loop, char* message, int r);
int on_timer_event(jsys_descriptor *timer);

int on_stdin_event(jsys_descriptor *client) {
  if (jsys_descriptor_is_readable(client)) {
    ssize_t bytes = 0;
    int count = 32;
    int fd = client->fd;
    while ((bytes = read(fd, buf, BUFSIZE))) {
      if (bytes == -1) {
        if (errno == EAGAIN) {
          fprintf(stderr, "count: %i\n", count);
          break;
        }
        perror("read");
        break;
      }
      total += (size_t)bytes;
      gtotal += (size_t)bytes;
      if (--count == 0) break;
    }
  }
  if (jsys_descriptor_is_error(client)) {
    jsys_loop* loop = client->loop;
    jsys_descriptor* timer = client->data;
    jsys_descriptor_free(timer);
    jsys_descriptor_free(client);
    jsys_loop_stop(loop);
    jsys_loop_free(loop);
    fprintf(stderr, "grand total: %lu bytes\n", gtotal);
  }
  return 0;
}

int on_stdout_event(jsys_descriptor *client) {
  if (jsys_descriptor_is_writable(client)) {
    jsys_descriptor *tty_stdin = jsys_tty_create(client->loop, STDIN_FILENO);
    if (tty_stdin == NULL) return dump_error(client->loop, "could not open stdout", 1);
    tty_stdin->data = client->data;
    client->data = tty_stdin;
    tty_stdin->callback = on_stdin_event;
    int r = jsys_loop_add(client->loop, tty_stdin);
    if (r == -1) return dump_error(client->loop, "loop_add", r);
  }
  if (jsys_descriptor_is_error(client)) {
    jsys_descriptor* tty_stdin = (jsys_descriptor*)client->data;
    close(tty_stdin->fd);
    jsys_descriptor_free(client);
  }
  return 0;
}

int dump_error(jsys_loop* loop, char* message, int r) {
  fprintf(stderr, "error: %s (%i):\n%s\n", message, r, strerror(errno));
  return r;
}

int on_timer_event(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  fprintf(stderr, "total: %lu Kb\n", (total / (1024)) * 8);
  total = 0;
  return 0;
}

int main() {
  errno = 0;
  int r = 0;

  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return jsys_dump_error(loop, "loop_init", 0);

  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return dump_error(loop, "timer_create", 0);
  timer->callback = on_timer_event;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return dump_error(loop, "timer_loop_add", r);

  //jsys_descriptor *tty_stdout = jsys_tty_create(STDOUT_FILENO);
  //if (tty_stdout == NULL) dump_error(loop, "could not open stdout", 1);
  //tty_stdout->data = timer;
  //tty_stdout->callback = on_stdout_event;

  //r = jsys_loop_add_flags(loop, tty_stdout, EPOLLOUT);
  //if (r == -1) return dump_error(loop, "stdout_loop_add", r);

  r = jsys_loop_run(loop);
  if (r == -1) return dump_error(loop, "loop_run", r);
  return 0;
}
