#include "jsys.h"
#include <pthread.h>
#include <syscall.h>

__thread uint64_t counter = 0;
_Atomic int running = 0;

int on_timer_event(jsys_descriptor *timer);
void* thread_func(void* arg);

int on_timer_event(jsys_descriptor *timer) {
  jsys_loop* loop = timer->loop;
  int descriptors = 0;
  for (int i = 0; i < loop->maxfds; i++) {
    if (loop->descriptors[i] != NULL) {
      descriptors++;
    }
  }
  fprintf(stderr, "thread: %li loop: %i expirations: %lu counter: %lu descriptors: %i, threads: %i\n", syscall(SYS_gettid), loop->fd, jsys_timer_read(timer), counter++, descriptors, running);
  return 0;
}

void* thread_func(void* arg) {
  int r = 0;
  running++;
  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return (void *)-1;
  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return (void *)-1;
  timer->callback = on_timer_event;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return (void *)-1;
  r = jsys_loop_run(loop);
  if (r == -1) return (void *)-1;
  running--;
  return 0;
}

int main(int argc, char** argv) {
  void* tret;
  int threads = 1;
  if (argc > 1) {
    threads = atoi(argv[1]);
  }
  pthread_t pool[threads];
  for (int i = 0; i < threads; i++) {
    int r = pthread_create(&pool[i], NULL, thread_func, NULL);
    if (r) {
      fprintf(stderr, "pthread_create: %i\n", r);
    }
  }
  for (int i = 0; i < threads; i++) {
    int r = pthread_join(pool[i], &tret);
    if (r) {
      fprintf(stderr, "pthread_join: %i\n", r);
    } else {
      fprintf(stderr, "thread %i status: %li\n", i, (long)tret);
    }
  }
  return 0;
}
