#include "jsys.h"

static uint64_t counter = 0;

int on_timer_event(jsys_descriptor *timer);

int on_timer_event(jsys_descriptor *timer) {
  jsys_loop* loop = timer->loop;
  fprintf(stderr, "loop: %i expirations: %lu counter: %lu\n", loop->fd, jsys_timer_read(timer), counter++);
  for (int i = 0; i < loop->maxfds; i++) {
    if (loop->descriptors[i] != NULL) {
      fprintf(stderr, "%s (%i)\n", jsys_descriptor_name(loop->descriptors[i]), loop->descriptors[i]->fd);
    }
  }
  return 0;
}

int main() {
  int r = 0;

  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return jsys_dump_error(loop, "loop_create", 0);

  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return jsys_dump_error(loop, "timer_create", 0);
  timer->callback = on_timer_event;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return jsys_dump_error(loop, "timer_loop_add", r);

  jsys_loop* loop2 = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);

  jsys_descriptor *timer2 = jsys_timer_create(loop2, 1000000000, 1000000000);
  if (timer2 == NULL) return jsys_dump_error(loop2, "timer_create", 0);
  timer2->callback = on_timer_event;
  r = jsys_loop_add(loop2, timer2);
  if (r == -1) return jsys_dump_error(loop2, "timer_loop_add", r);

  while (1) {
    r = jsys_loop_run_once(loop, 1); // wait 1ms for events
    if (r == -1) return jsys_dump_error(loop, "loop_run_once", r);
    r = jsys_loop_run_once(loop2, 1);
    if (r == -1) return jsys_dump_error(loop2, "loop_run_once", r);
  }

  return 0;
}
