#include "jsys.h"

static uint64_t counter = 0;

ssize_t process_memory_usage(void);
int on_timer_event1(jsys_descriptor *timer);
int on_timer_event2(jsys_descriptor *timer);

ssize_t process_memory_usage() {
  char buf[1024];
  const char* s;
  ssize_t n;
  long val;
  int fd;
  int i;
  do {
    fd = open("/proc/self/stat", O_RDONLY);
  } while (fd == -1 && errno == EINTR);
  if (fd == -1) return -1;
  do
    n = read(fd, buf, sizeof(buf) - 1);
  while (n == -1 && errno == EINTR);
  close(fd);
  if (n == -1)
    return (ssize_t)errno;
  buf[n] = '\0';
  s = strchr(buf, ' ');
  if (s == NULL)
    goto err;
  s += 1;
  if (*s != '(')
    goto err;
  s = strchr(s, ')');
  if (s == NULL)
    goto err;
  for (i = 1; i <= 22; i++) {
    s = strchr(s + 1, ' ');
    if (s == NULL)
      goto err;
  }
  errno = 0;
  val = strtol(s, NULL, 10);
  if (errno != 0)
    goto err;
  if (val < 0)
    goto err;
  return val * getpagesize();
err:
  return 0;
}

int on_timer_event1(jsys_descriptor *timer) {
  fprintf(stderr, "expirations: %lu, rss: %lu, counter: %lu\n", jsys_timer_read(timer), process_memory_usage(), counter);
  return 0;
}

int on_timer_event2(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  counter++;
  if (counter > 50000) {
    jsys_loop_free(timer->loop);
  }
  return 0;
}

int main() {
  int r = 0;

  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return jsys_dump_error(loop, "loop_init", 0);

  jsys_descriptor *timer1 = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer1 == NULL) return jsys_dump_error(loop, "timer_create", 0);
  timer1->callback = on_timer_event1;
  r = jsys_loop_add(loop, timer1);
  if (r == -1) return jsys_dump_error(loop, "timer_loop_add", r);

  jsys_descriptor *timer2 = jsys_timer_create(loop, 100000, 100000);
  if (timer2 == NULL) return jsys_dump_error(loop, "timer_create", 0);
  timer2->callback = on_timer_event2;
  r = jsys_loop_add(loop, timer2);
  if (r == -1) return jsys_dump_error(loop, "timer_loop_add", r);

  r = jsys_loop_run(loop);
  if (r == -1) return jsys_dump_error(loop, "loop_run", r);
  return 0;
}
