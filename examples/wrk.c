#include "jsys_http.h"
#include <pthread.h>
#include <sys/time.h>
#include <getopt.h>
#include <inttypes.h>

#define BUFSIZE 65536
#define MAXHEADERS 32
#define OUTBUFFERS 1024

__thread jsys_descriptor* signal_descriptor;

typedef struct {
    int scale;
    char *base;
    char *units[];
} units;

typedef struct {
  uint64_t start;
  uint64_t now;
  uint64_t finish;
  uint64_t rps;
  uint64_t bps;
  uint64_t requests;
  uint64_t responses;
  int connections;
  int duration;
} http_stats;

static struct config {
    uint64_t connections;
    uint64_t duration;
    uint64_t threads;
    uint64_t timeout;
    uint64_t pipeline;
} cfg;

static struct option longopts[] = {
    { "connections", required_argument, NULL, 'c' },
    { "duration",    required_argument, NULL, 'd' },
    { "threads",     required_argument, NULL, 't' },
    { "timeout",     required_argument, NULL, 'T' },
    { NULL,          0,                 NULL,  0  }
};

int httpd_on_connect(jsys_descriptor *client);
int httpd_on_headers(jsys_descriptor *client);
int httpd_on_body(jsys_descriptor *client, char* chunk, size_t len);
int httpd_on_response(jsys_descriptor *client);
int httpd_on_end(jsys_descriptor *client);
int process_on_signal(jsys_descriptor* signal);
int process_on_timer(jsys_descriptor *timer);

static uint64_t time_us() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return (t.tv_sec * 1000000) + t.tv_usec;
}

int httpd_on_data(jsys_descriptor *client, size_t len) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  if (context->current_buffer == 0) return;
  int r = 0;
  if (context->current_buffer == 1) {
    r = jsys_tcp_write(client, &context->out[context->current_buffer]);
  } else {
    r = jsys_tcp_writev(client, context->out, context->current_buffer);
  }
  context->current_buffer = 0;
  return 0;
}

int httpd_on_connect(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  jsys_httpd_settings* http_settings = (jsys_httpd_settings *)context->settings->data;
  http_stats* stats = (http_stats*)http_settings->data;
  context->in = (struct iovec*)calloc(1, sizeof(struct iovec));
  context->out = (struct iovec*)calloc(OUTBUFFERS, sizeof(struct iovec));
  jsys_tcp_set_nodelay(client, 1);
  int r = jsys_loop_mod_flags(client, EPOLLIN | EPOLLET);
  if (r == -1) return r;
  void* rGET = (void*)"GET / HTTP/1.1\r\nHost: foo\r\n\r\n";
  int len = strlen((char*)rGET);
  int i = 0;
  while (i < OUTBUFFERS) {
    context->out[i].iov_base = rGET;
    context->out[i].iov_len = len;
    i++;
  }
  context->current_buffer = 0;
  context->in->iov_base = calloc(1, http_settings->buffer_size);
  context->in->iov_len = http_settings->buffer_size;
  r = jsys_tcp_writev(client, context->out, 1);
  if (r == -1) return -1;
  stats->requests++;
  return 0;
}

int httpd_on_headers(jsys_descriptor *client) {
  return 0;
}

int httpd_on_body(jsys_descriptor *client, char* chunk, size_t len) {
  return 0;
}

int httpd_on_response(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  jsys_httpd_settings* http_settings = (jsys_httpd_settings *)context->settings->data;
  jsys_http_client_context* http = (jsys_http_client_context*)context->data;
  http_stats* stats = (http_stats*)http_settings->data;
  context->current_buffer++;
  stats->rps++;
  stats->responses++;
  stats->bps += (http->header_size + http->body_bytes);
  if (stats->now - stats->start > (stats->duration * 1000000)) client->closing = 1;
  if (context->current_buffer == OUTBUFFERS) {
    int r = jsys_tcp_writev(client, context->out, context->current_buffer);
    if (r == -1) return -1;
    stats->requests += context->current_buffer;
    context->current_buffer = 0;
  }
  return 0;
}

int httpd_on_end(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  free(context->in);
  free(context->out);
  //jsys_descriptor_free(client);
  return 0;
}

int process_on_signal(jsys_descriptor* signal) {
	struct signalfd_siginfo info;
	ssize_t r = read(signal->fd, &info, sizeof info);
	if (r != sizeof info) return -1;
  return 0;
}

int process_on_timer(jsys_descriptor *timer) {
  jsys_loop* loop = timer->loop;
  http_stats* stats = (http_stats*)timer->data;
  stats->now = time_us();
  jsys_timer_read(timer);
  int count = 0;
  for (int i = 0; i < loop->maxfds; i++) {
    if (loop->descriptors[i] != NULL) {
      count++;
    }
  }
  // TODO: divide by time since last timer event to get more accurate number
  fprintf(stderr, "req: %lu, res: %lu, rps: %lu, bps: %lu, descriptors: %i\n", stats->requests, stats->responses, stats->rps, stats->bps, count);
  if (count == 2) {
    jsys_descriptor_free(signal_descriptor);
    jsys_descriptor_free(timer);
    jsys_loop_free(loop);
  }
  stats->rps = stats->bps = 0;
  return 0;
}

int create_client(int port, char* host, jsys_httpd_settings* http_settings, jsys_loop* loop) {
  jsys_descriptor* client = jsys_http_create_client(loop, http_settings);
  int r = jsys_tcp_connect(client, port, host);
  if (r == -1) return -1;
  r = jsys_loop_add_flags(loop, client, EPOLLOUT);
  if (r == -1) return -1;
  return 0;
}

void* thread_func(void* arg) {
  int r = 0;
  http_stats* stats = (http_stats*)arg;
  stats->requests = stats->responses = stats->bps = stats->rps = 0;

  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return (void *)-1;

  jsys_signal_add(loop, SIGTERM);
  jsys_signal_add(loop, SIGHUP);
  jsys_signal_add(loop, SIGPIPE);
  signal_descriptor = jsys_signal_watcher_create(loop);
  if (signal_descriptor == NULL) return (void *)-1;
  r = jsys_loop_add_flags(loop, signal_descriptor, EPOLLIN);
  if (r == -1) return (void *)-1;
  signal_descriptor->callback = process_on_signal;

  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return (void *)-1;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return (void *)-1;
  timer->callback = process_on_timer;

  jsys_httpd_settings* http_settings = (jsys_httpd_settings *)calloc(1, sizeof(jsys_httpd_settings));
  http_settings->on_headers = httpd_on_headers;
  http_settings->on_response = httpd_on_response;
  http_settings->on_body = httpd_on_body;
  http_settings->on_connect = httpd_on_connect;
  http_settings->on_end = httpd_on_end;
  http_settings->on_data = httpd_on_data;
  http_settings->buffer_size = BUFSIZE;
  http_settings->max_headers = MAXHEADERS;
  http_settings->domain = AF_INET;
  http_settings->type = SOCK_STREAM;
  http_settings->data = stats;
  timer->data = stats;
  stats->start = stats->now = time_us();

  for (int i=0; i < stats->connections; i++) {
    create_client(3000, "127.0.0.1", http_settings, loop);
  }
  r = jsys_loop_run(loop);
  stats->finish = time_us();

  if (r == -1) return (void *)-1;
  return (void *)0;
}

static int scan_value(char *s, uint64_t *n) {
  uint64_t base, scale = 1;
  char unit[3] = { 0, 0, 0 };
  int c;
  if ((c = sscanf(s, "%"SCNu64"%2s", &base, unit)) < 1) return -1;
  *n = base * scale;
  return 0;
}

int main(int argc, char** argv) {
  void* tret;

  struct config *config = &cfg;
  memset(config, 0, sizeof(struct config));
  config->threads     = 1;
  config->connections = 1;
  config->duration    = 3;
  config->timeout     = 2;
  int c;
  
  while ((c = getopt_long(argc, argv, "t:c:d:T:", longopts, NULL)) != -1) {
    switch (c) {
      case 't':
        if (scan_value(optarg, &config->threads)) return -1;
        break;
      case 'c':
        if (scan_value(optarg, &config->connections)) return -1;
        break;
      case 'd':
        if (scan_value(optarg, &config->duration)) return -1;
        break;
      case 'T':
        if (scan_value(optarg, &config->timeout)) return -1;
        config->timeout *= 1000;
        break;
      default:
        return -1;
    }
  }
  http_stats stats[config->threads];
  pthread_t pool[config->threads];
  for (int i = 0; i < config->threads; i++) {
    stats[i].connections = config->connections / config->threads;
    stats[i].duration = config->duration;
    pthread_create(&pool[i], NULL, thread_func, &stats[i]);
  }
  for (int i = 0; i < config->threads; i++) {
    pthread_join(pool[i], &tret);
    //fprintf(stderr, "thread %i status: %li\n", i, (long)tret);
  }
  fprintf(stderr, "--------------------------------------------------------------------------------\n");
  uint64_t total_responses = 0;
  uint64_t microseconds_elapsed = 0;
  for (int i = 0; i < config->threads; i++) {
    microseconds_elapsed = stats[i].finish - stats[i].start;
    fprintf(stderr, "req: %lu, res: %lu, maxrps: %lu, maxbps: %lu, time: %lu\n", stats[i].requests, stats[i].responses, stats[i].rps, stats[i].bps, microseconds_elapsed);
    total_responses += stats[i].responses;
  }
  fprintf(stderr, "total: %lu, rps: %lu\n", total_responses, (total_responses / (microseconds_elapsed / 1000000)));
  fprintf(stderr, "--------------------------------------------------------------------------------\n");
  return 0;
}
