#include "jsys_http.h"
#include <pthread.h>
#include <sys/time.h>

#define BUFSIZE 65536
#define MAXHEADERS 32
#define OUTBUFFERS 1024

__thread int requests;
__thread int conn;
__thread uint64_t then;

int httpd_on_connect(jsys_descriptor *client);
int httpd_on_headers(jsys_descriptor *client);
int httpd_on_body(jsys_descriptor *client, char* chunk, size_t len);
int httpd_on_request(jsys_descriptor *client);
int httpd_on_end(jsys_descriptor *client);
int process_on_signal(jsys_descriptor* signal);
int process_on_timer(jsys_descriptor *timer);

static uint64_t time_us() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return (t.tv_sec * 1000000) + t.tv_usec;
}

/*
  HTTP ondata callback. called at end of each chunk on stream
*/
int httpd_on_data(jsys_descriptor *client, size_t len) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  if (context->current_buffer == 0) return;
  int r = 0;
  if (context->current_buffer == 1) {
    r = jsys_tcp_write(client, &context->out[context->current_buffer]);
  } else {
    r = jsys_tcp_writev(client, context->out, context->current_buffer);
  }
  context->current_buffer = 0;
  return 0;
}

/*
  HTTP connection callback
*/
int httpd_on_connect(jsys_descriptor *client) {
#ifdef TRACE
  fprintf(stderr, "httpd_on_connect (%i)\n", client->fd);
#endif
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  jsys_httpd_settings* http_settings = (jsys_httpd_settings *)context->settings->data;
  context->in = (struct iovec*)calloc(1, sizeof(struct iovec));
  context->out = (struct iovec*)calloc(OUTBUFFERS, sizeof(struct iovec));
  char* r200 = "HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n";
  int len = strlen(r200);
  int i = 0;
  while (i < OUTBUFFERS) {
    context->out[i].iov_base = (void*)r200;
    context->out[i].iov_len = len;
    i++;
  }
  context->current_buffer = 0;
  context->in->iov_base = calloc(1, http_settings->buffer_size);
  context->in->iov_len = http_settings->buffer_size;
  conn++;
  return 0;
}

/*
  HTTP headers callback
*/
int httpd_on_headers(jsys_descriptor *client) {
#ifdef TRACE
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  jsys_http_server_context* http = (jsys_http_server_context*)context->data;
  fprintf(stderr, "httpd_on_headers (%i), size: (%lu)\n", client->fd, http->header_size);
  fprintf(stderr, "  method   : %.*s\n", (int)http->method_len, http->method);
  fprintf(stderr, "  path     : %.*s\n", (int)http->path_len, http->path);
  fprintf(stderr, "  version  : 1.%i\n", http->minor_version);
  fprintf(stderr, "  body     : %lu\n", http->body_length);
  fprintf(stderr, "  bytes    : %lu\n", http->body_bytes);
  fprintf(stderr, "  headers  :\n");
  size_t i = 0;
  while (i < http->num_headers) {
    fprintf(stderr, "    %.*s : %.*s\n", (int)http->headers[i].name_len, http->headers[i].name, (int)http->headers[i].value_len, http->headers[i].value);
    i++;
  }
#endif
  return 0;
}

/*
  HTTP body chunk callback
*/
int httpd_on_body(jsys_descriptor *client, char* chunk, size_t len) {
#ifdef TRACE
  fprintf(stderr, "httpd_on_body (%i), size: (%lu)\n", client->fd, len);
#endif
  return 0;
}

/*
  HTTP request callback (body fully read)
*/
int httpd_on_request(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
#ifdef TRACE
  jsys_http_server_context* http = (jsys_http_server_context*)context->data;
  fprintf(stderr, "httpd_on_request (%i), size: (%lu)\n", client->fd, http->header_size);
  fprintf(stderr, "  method   : %.*s\n", (int)http->method_len, http->method);
  fprintf(stderr, "  path     : %.*s\n", (int)http->path_len, http->path);
  fprintf(stderr, "  version  : 1.%i\n", http->minor_version);
  fprintf(stderr, "  body     : %lu\n", http->body_length);
  fprintf(stderr, "  bytes    : %lu\n", http->body_bytes);
  fprintf(stderr, "  headers  :\n");
  size_t i = 0;
  while (i < http->num_headers) {
    fprintf(stderr, "    %.*s : %.*s\n", (int)http->headers[i].name_len, http->headers[i].name, (int)http->headers[i].value_len, http->headers[i].value);
    i++;
  }
#endif
  context->current_buffer++;
  if (context->current_buffer == OUTBUFFERS) {
#ifdef TRACE
    fprintf(stderr, "httpd_on_request.writev: %i\n", context->current_buffer);
#endif
    int r = jsys_tcp_writev(client, context->out, context->current_buffer);
    if (r == -1) return -1;
    context->current_buffer = 0;
  }
  requests++;
  return 0;
}

/*
  HTTP connection close callback
*/
int httpd_on_end(jsys_descriptor *client) {
#ifdef TRACE
  fprintf(stderr, "httpd_on_end (%i)\n", client->fd);
#endif
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  free(context->in);
  free(context->out);
  conn--;
  return 0;
}

/*
  Signal Handler
*/
int process_on_signal(jsys_descriptor* signal) {
	struct signalfd_siginfo info;
  fprintf(stderr, "hello\n");
	ssize_t r = read(signal->fd, &info, sizeof info);
	if (r != sizeof info) return -1;
#ifdef TRACE
	fprintf(stderr, "on_signal %s (%u)\n", strsignal((int)info.ssi_signo), info.ssi_signo);
	fprintf(stderr, "  signo   = %u\n", info.ssi_signo);
	fprintf(stderr, "  errno   = %i\n", info.ssi_errno);
	fprintf(stderr, "  code    = %i\n", info.ssi_code);
	fprintf(stderr, "  pid     = %u\n", info.ssi_pid);
	fprintf(stderr, "  uid     = %u\n", info.ssi_uid);
	fprintf(stderr, "  fd      = %i\n", info.ssi_fd);
	fprintf(stderr, "  tid     = %u\n", info.ssi_tid);
	fprintf(stderr, "  band    = %u\n", info.ssi_band);
	fprintf(stderr, "  overrun = %u\n", info.ssi_overrun);
	fprintf(stderr, "  trapno  = %u\n", info.ssi_trapno);
	fprintf(stderr, "  status  = %i\n", info.ssi_status);
	fprintf(stderr, "  int     = %i\n", info.ssi_int);
	fprintf(stderr, "  ptr     = %lu\n", info.ssi_ptr);
	fprintf(stderr, "  utime   = %lu\n", info.ssi_utime);
	fprintf(stderr, "  stime   = %lu\n", info.ssi_stime);
	fprintf(stderr, "  addr    = %lu\n", info.ssi_addr);
#endif
  return 0;
}

/*
  Timer callback
*/
int process_on_timer(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  uint64_t now = time_us();
  uint64_t diff = now - then;
  then = now;
  double rps = (double)requests / ((double)diff / 1000000);
  fprintf(stderr, "conn: %i rps: %f, time: %lu\n", conn, rps, diff);
#ifdef TRACE
  jsys_loop* loop = timer->loop;
  uint64_t expirations = jsys_timer_read(timer);
  fprintf(stderr, "on_timer_event (%i), expirations: (%lu)\n", timer->fd, expirations);
  fprintf(stderr, "descriptors:\n");
  for (int i = 0; i < loop->maxfds; i++) {
    if (loop->descriptors[i] != NULL) {
      fprintf(stderr, "  %s (%i)\n", jsys_descriptor_name(loop->descriptors[i]), loop->descriptors[i]->fd);
    }
  }
#endif
  requests = 0;
  return 0;
}

void* thread_func(void* arg) {
  int r = 0;

  // create the loop
  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return (void *)-1;

  // create signal handlers
  jsys_signal_add(loop, SIGTERM);
  jsys_signal_add(loop, SIGHUP);
  jsys_signal_add(loop, SIGPIPE);
  jsys_descriptor *signal = jsys_signal_watcher_create(loop);
  if (signal == NULL) return (void *)-1;
  r = jsys_loop_add_flags(loop, signal, EPOLLIN); // level triggered
  if (r == -1) return (void *)-1;
  signal->callback = process_on_signal;

  // create a timer and set to fire every second
  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return (void *)-1;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return (void *)-1;
  timer->callback = process_on_timer;

  // create the httpd settings
  jsys_httpd_settings* http_settings = (jsys_httpd_settings *)calloc(1, sizeof(jsys_httpd_settings));
  // callbacks
  http_settings->on_headers = httpd_on_headers;
  http_settings->on_request = httpd_on_request;
  http_settings->on_body = httpd_on_body;
  http_settings->on_connect = httpd_on_connect;
  http_settings->on_end = httpd_on_end;
  http_settings->on_data = httpd_on_data;
  // read buffer size
  http_settings->buffer_size = BUFSIZE;
  // max number of headers
  http_settings->max_headers = MAXHEADERS;
  // socket domain and type
  http_settings->domain = AF_INET;
  http_settings->type = SOCK_STREAM;

  // create the server
  jsys_descriptor* server = jsys_http_create_server(loop, http_settings);

  // bind to port 3000 on 0.0.0.0
  r = jsys_tcp_bind_reuse(server, 3000, INADDR_ANY);
  if (r == -1) return (void *)-1;
  // listen
  r = jsys_tcp_listen(server, SOMAXCONN);
  if (r == -1) return (void *)-1;
  // add server descriptor to event loop
  r = jsys_loop_add(loop, server);
  if (r == -1) return (void *)-1;

  then = time_us();
  // run the loop continually
  r = jsys_loop_run(loop);
  if (r == -1) return (void *)-1;
  return (void *)0;
}

int main(int argc, char** argv) {
  void* tret;
  int threads = 1;
  if (argc > 1) {
    threads = atoi(argv[1]);
  }
  pthread_t pool[threads];
  for (int i = 0; i < threads; i++) {
    pthread_create(&pool[i], NULL, thread_func, NULL);
  }
  for (int i = 0; i < threads; i++) {
    pthread_join(pool[i], &tret);
    fprintf(stderr, "thread %i status: %li\n", i, (long)tret);
  }
  return 0;
}
