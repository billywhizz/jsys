#include "jsys_http.h"
#include <pthread.h>

#define BUFSIZE 4096
#define MAXHEADERS 32
#define OUTBUFFERS 1

__thread int requests;

int httpd_on_connect(jsys_descriptor *client);
int httpd_on_headers(jsys_descriptor *client);
int httpd_on_body(jsys_descriptor *client, char* chunk, size_t len);
int httpd_on_response(jsys_descriptor *client);
int httpd_on_end(jsys_descriptor *client);
ssize_t process_memory_usage();

ssize_t process_memory_usage() {
  char buf[1024];
  const char* s = NULL;
  ssize_t n = 0;
  long val = 0;
  int fd = 0;
  int i = 0;
  do {
    fd = open("/proc/self/stat", O_RDONLY);
  } while (fd == -1 && errno == EINTR);
  if (fd == -1) return (ssize_t)errno;
  do
    n = read(fd, buf, sizeof(buf) - 1);
  while (n == -1 && errno == EINTR);
  close(fd);
  if (n == -1)
    return (ssize_t)errno;
  buf[n] = '\0';
  s = strchr(buf, ' ');
  if (s == NULL)
    goto err;
  s += 1;
  if (*s != '(')
    goto err;
  s = strchr(s, ')');
  if (s == NULL)
    goto err;
  for (i = 1; i <= 22; i++) {
    s = strchr(s + 1, ' ');
    if (s == NULL)
      goto err;
  }
  errno = 0;
  val = strtol(s, NULL, 10);
  if (errno != 0)
    goto err;
  if (val < 0)
    goto err;
  return val * getpagesize();
err:
  return 0;
}

int httpd_on_connect(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  jsys_httpd_settings* http_settings = (jsys_httpd_settings *)context->settings->data;
  context->in = (struct iovec*)calloc(1, sizeof(struct iovec));
  context->out = (struct iovec*)calloc(OUTBUFFERS, sizeof(struct iovec));
  jsys_tcp_set_nodelay(client, 1);
  int r = jsys_loop_mod_flags(client, EPOLLIN | EPOLLET);
  if (r == -1) return r;
  void* rGET = (void*)"GET / HTTP/1.1\r\nHost: rdrop.com\r\n\r\n";
  //void* rGET = (void*)"GET /users/paulmck/scalability/paper/whymb.2010.07.23a.pdf HTTP/1.1\r\nHost: rdrop.com\r\n\r\n";
  int len = strlen((char*)rGET);
  int i = 0;
  while (i < OUTBUFFERS) {
    context->out[i].iov_base = rGET;
    context->out[i].iov_len = len;
    i++;
  }
  context->current_buffer = 0;
  context->in->iov_base = calloc(1, http_settings->buffer_size);
  context->in->iov_len = http_settings->buffer_size;
  r = jsys_tcp_writev(client, context->out, 1);
  if (r == -1) return -1;
  return 0;
}

int httpd_on_headers(jsys_descriptor *client) {
/*
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  jsys_http_client_context* http = (jsys_http_client_context*)context->data;
  fprintf(stderr, "httpd_on_headers (%i), size: (%lu)\n", client->fd, http->header_size);
  fprintf(stderr, "  status   : %u %.*s\n", http->status, (int)http->status_message_len, http->status_message);
  fprintf(stderr, "  version  : 1.%i\n", http->minor_version);
  fprintf(stderr, "  body     : %lu\n", http->body_length);
  fprintf(stderr, "  bytes    : %lu\n", http->body_bytes);
  fprintf(stderr, "  headers  : %lu\n", http->num_headers);
  size_t i = 0;
  while (i < http->num_headers) {
    fprintf(stderr, "    %.*s : %.*s\n", (int)http->headers[i].name_len, http->headers[i].name, (int)http->headers[i].value_len, http->headers[i].value);
    i++;
  }
*/
  return 0;
}

int httpd_on_body(jsys_descriptor *client, char* chunk, size_t len) {
  //fprintf(stderr, "body: %lu\n", len);
  return 0;
}

int httpd_on_response(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  //jsys_http_client_context* http = (jsys_http_client_context*)context->data;
  //fprintf(stderr, "httpd_on_response (%i), size: (%lu)\n", client->fd, http->header_size);
  //fprintf(stderr, "  body     : %lu\n", http->body_length);
  //fprintf(stderr, "  bytes    : %lu\n", http->body_bytes);
  //jsys_tcp_shutdown(client);
  requests++;
  jsys_tcp_writev(client, context->out, 1);
  return 0;
}

int create_client(int port, char* host, jsys_httpd_settings* http_settings, jsys_loop* loop) {
  jsys_descriptor* client = jsys_http_create_client(loop, http_settings);
  int r = jsys_tcp_connect(client, port, host);
  if (r == -1) return -1;
  r = jsys_loop_add_flags(loop, client, EPOLLOUT);
  if (r == -1) return -1;
  return 0;
}

int httpd_on_end(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  free(context->in);
  free(context->out);
/*
  int count = 0;
  jsys_loop* loop = client->loop;
  for (int i = 0; i < MAXFDS; i++) {
    if (loop->descriptors[i] != NULL) {
      count++;
    }
  }
  //fprintf(stderr, "descriptors: %i\n", count);
  if (count == 1) {
    //jsys_httpd_settings* http_settings = (jsys_httpd_settings *)context->settings->data;
    //jsys_httpd_settings* http_settings_new = calloc(1, sizeof(jsys_httpd_settings));
    //memcpy(http_settings_new, http_settings, sizeof(jsys_httpd_settings));
    jsys_loop_free(loop);
    //int r = create_client(80, "199.26.172.35", http_settings_new, loop);
    //if (r == -1) return jsys_dump_error(loop, "create_client", r);
  }
*/
  return 0;
}

int on_timer_event(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  ssize_t mem = process_memory_usage();
  fprintf(stderr, "\x1B[36mrps\x1B[0m %i \x1B[36mmem\x1B[0m %lu\n", requests, mem);
  requests = 0;
  return 0;
}

int run(int clients) {
  int r = 0;
  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return jsys_dump_error(loop, "loop_create", 1);
  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return jsys_dump_error(loop, "timer_create", 1);
  r = jsys_loop_add(loop, timer);
  if (r == -1) return jsys_dump_error(loop, "timer_loop_add", r);
  timer->callback = on_timer_event;
  jsys_httpd_settings* http_settings = (jsys_httpd_settings *)calloc(1, sizeof(jsys_httpd_settings));
  http_settings->on_headers = httpd_on_headers;
  http_settings->on_response = httpd_on_response;
  http_settings->on_body = httpd_on_body;
  http_settings->on_connect = httpd_on_connect;
  http_settings->on_end = httpd_on_end;
  http_settings->buffer_size = BUFSIZE;
  http_settings->max_headers = MAXHEADERS;
  http_settings->domain = AF_INET;
  http_settings->type = SOCK_STREAM;
  //r = create_client(80, "199.26.172.35", http_settings, loop);
  for (int i = 0; i < clients; i++) {
    r = create_client(3000, "127.0.0.1", http_settings, loop);
    if (r == -1) return jsys_dump_error(loop, "create_client", 1);
  }
  r = jsys_loop_run(loop);
  if (r == -1) return jsys_dump_error(loop, "loop_run", 1);
  return r;
}

void* thread_func(void* arg) {
  run(*(int*)arg);
  pthread_exit(0);
}

int main(int argc, char** argv) {
  void* tret;
  int threads = 1;
  int clients = 1;
  if (argc > 1) {
    threads = atoi(argv[1]);
  }
  if (argc > 2) {
    clients = atoi(argv[2]);
  }
  pthread_t pool[threads];
  for (int i = 0; i < threads; i++) {
    pthread_create(&pool[i], NULL, thread_func, &clients);
  }
  for (int i = 0; i < threads; i++) {
    pthread_join(pool[i], &tret);
    fprintf(stderr, "thread %i status: %li\n", i, (long)tret);
  }
  return 0;
}
