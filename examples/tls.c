#include "jsys.h"
#include <pthread.h>
#include <openssl/ssl.h>

#define BUFSIZE 65536

static jsys_loop *loop;

__thread SSL_CTX* ctx;

int on_client_data(jsys_descriptor *client, size_t bytes);
int on_timer_event(jsys_descriptor *timer);
int on_client_connect(jsys_descriptor* client);
int on_client_end(jsys_descriptor* client);
int on_signal(jsys_descriptor* signal);


static void
dump_cert_info(SSL *ssl) {
  fprintf(stderr, "server version: %s\n", SSL_get_version(ssl));
  fprintf(stderr, "using cipher %s\n", SSL_get_cipher(ssl));
  X509 *server_cert = SSL_get_peer_certificate(ssl);
  if (server_cert != NULL) {
    char *str = X509_NAME_oneline(X509_get_subject_name(server_cert), 0, 0);
    if(str == NULL) fprintf(stderr, "warn X509 subject name is null\n");
    fprintf(stderr, "Subject: %s\n", str);
    OPENSSL_free(str);
    str = X509_NAME_oneline(X509_get_issuer_name(server_cert), 0, 0);
    if(str == NULL) fprintf(stderr, "warn X509 issuer name is null\n");
    fprintf(stderr, "Issuer: %s\n", str);
    OPENSSL_free(str);
    X509_free(server_cert);
  } else {
    fprintf(stderr, "server does not have certificate.\n");
  }
}

int on_client_data(jsys_descriptor *client, size_t bytes) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  fprintf(stderr, "client_data: (%i) %lu\n%.*s\n", client->fd, bytes, (int)bytes, (char*)context->in->iov_base);
  return 0;
}

int on_timer_event(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  jsys_loop* loop = timer->loop;
  fprintf(stderr, "timer\n");
  for (int i = 0; i < loop->maxfds; i++) {
    if (loop->descriptors[i] != NULL) {
      fprintf(stderr, "descriptor (%s) %i\n", jsys_descriptor_name(loop->descriptors[i]), loop->descriptors[i]->fd);
    }
  }
  return 0;
}

int on_client_connect(jsys_descriptor* client) {
  fprintf(stderr, "client_connect: (%i)\n", client->fd);
  int r = jsys_loop_mod_flags(client, EPOLLIN | EPOLLET);
  if (r == -1) return r;
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  context->in = calloc(1, sizeof(struct iovec));
  context->out = calloc(1, sizeof(struct iovec));
  context->in->iov_base = calloc(1, BUFSIZE);
  context->in->iov_len = BUFSIZE;
  context->out->iov_base = "GET / HTTP/1.1\r\nHost: www.amazon.co.uk\r\n\r\n";
  context->out->iov_len = strlen(context->out->iov_base);
  SSL* ssl = SSL_new(ctx);
  SSL_set_fd(ssl, client->fd);
  SSL_set_connect_state(ssl);
  context->data = ssl;
  int err = 0;
  for(;;) {
    int success = SSL_connect(ssl);
    if(success < 0) {
      err = SSL_get_error(ssl, success);
      if (err == SSL_ERROR_WANT_READ || err == SSL_ERROR_WANT_WRITE || err == SSL_ERROR_WANT_X509_LOOKUP) {
          continue;
      }
      else if(err == SSL_ERROR_ZERO_RETURN) {
        printf("SSL_connect: close notify received from peer");
        return -1;
      }
      else {
        printf("Error SSL_connect: %d", err);
        perror("perror: ");
        SSL_free(ssl);
        jsys_tcp_shutdown(client);
        return -1;
      }
    }
    else {
      dump_cert_info(ssl);
      break;
    }
  }
  return SSL_write(ssl, context->out->iov_base, context->out->iov_len);
}

int on_client_end(jsys_descriptor* client) {
  fprintf(stderr, "client_end: (%i)\n", client->fd);
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  free(context->in);
  free(context->out);
  free(context);
  return 0;
}

int on_signal(jsys_descriptor* signal) {
	struct signalfd_siginfo info;
	ssize_t r = read(signal->fd, &info, sizeof info);
	if (r != sizeof info) return -1;
	fprintf(stderr, "on_signal %s (%u)\n", strsignal((int)info.ssi_signo), info.ssi_signo);
	fprintf(stderr, "  signo   = %u\n", info.ssi_signo);
	fprintf(stderr, "  errno   = %i\n", info.ssi_errno);
	fprintf(stderr, "  code    = %i\n", info.ssi_code);
	fprintf(stderr, "  pid     = %u\n", info.ssi_pid);
	fprintf(stderr, "  uid     = %u\n", info.ssi_uid);
	fprintf(stderr, "  fd      = %i\n", info.ssi_fd);
	fprintf(stderr, "  tid     = %u\n", info.ssi_tid);
	fprintf(stderr, "  band    = %u\n", info.ssi_band);
	fprintf(stderr, "  overrun = %u\n", info.ssi_overrun);
	fprintf(stderr, "  trapno  = %u\n", info.ssi_trapno);
	fprintf(stderr, "  status  = %i\n", info.ssi_status);
	fprintf(stderr, "  int     = %i\n", info.ssi_int);
	fprintf(stderr, "  ptr     = %lu\n", info.ssi_ptr);
	fprintf(stderr, "  utime   = %lu\n", info.ssi_utime);
	fprintf(stderr, "  stime   = %lu\n", info.ssi_stime);
	fprintf(stderr, "  addr    = %lu\n", info.ssi_addr);
  return 0;
}

int tcp_on_client_event(jsys_descriptor *client) {
  int r = 0;
  if (jsys_descriptor_is_error(client)) {
    jsys_stream_context* context = (jsys_stream_context*)client->data;
    context->settings->on_end(client);
    return jsys_descriptor_free(client);
  }
  if (jsys_descriptor_is_writable(client)) {
    jsys_stream_settings* settings = (jsys_stream_settings*)client->data;
    jsys_stream_context* context = calloc(1, sizeof(jsys_stream_context));
    context->settings = settings;
    context->data = settings;
    client->data = context;
    client->closing = 0;
    r = context->settings->on_connect(client);
    if (r == -1) return jsys_descriptor_free(client);
  }
  if (jsys_descriptor_is_readable(client)) {
    jsys_stream_context* context = (jsys_stream_context*)client->data;
    ssize_t bytes = 0;
    char* next = (char*)context->in->iov_base;
    size_t len = context->in->iov_len;
    SSL* ssl = (SSL*)context->data;
    while ((bytes = SSL_read(ssl, next, len))) {
      if (bytes == -1) {
        if (errno == EAGAIN) {
          if (client->closing == 1) {
            jsys_tcp_shutdown(client);
          }
          return 0;
        }
        if(bytes == SSL_ERROR_WANT_READ || bytes == SSL_ERROR_WANT_WRITE || bytes == SSL_ERROR_WANT_X509_LOOKUP) {
          fprintf(stderr, "Read could not complete. Will be invoked later.\n");
        } else if(bytes == SSL_ERROR_ZERO_RETURN) {
          printf("SSL_read: close notify received from peer\n");
        } else {
          printf("Error during SSL_read\n");
        }
        perror("read");
        break;
      }
      r = context->settings->on_data(client, (size_t)bytes);
      if (r == -1) break;
      if (context->current_buffer > 0) {
        r = jsys_tcp_writev(client, context->out, context->current_buffer);
        if (r == -1) return -1;
      }
      next = (char*)context->in->iov_base;
      len = context->in->iov_len;
    }
    context->settings->on_end(client);
    return jsys_descriptor_free(client);
  }
  return r;
}

void* thread_func(void* arg) {
  int r = 0;
  loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return (void *)-1;

  const SSL_METHOD *meth;
  meth = TLSv1_2_client_method();
  ctx = SSL_CTX_new(meth);
  SSL_CTX_set_cipher_list(ctx, "AES128-GCM-SHA256");
  SSL_CTX_set_options(ctx, SSL_OP_NO_COMPRESSION);

  jsys_signal_add(loop, SIGPIPE);
  jsys_descriptor *signal = jsys_signal_watcher_create(loop);
  if (signal == NULL) return (void *)-1;
  r = jsys_loop_add_flags(loop, signal, EPOLLIN); // level triggered
  if (r == -1) return (void *)-1;
  signal->callback = on_signal;

  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return (void *)-1;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return (void *)-1;
  timer->callback = on_timer_event;

  jsys_stream_settings* settings = calloc(1, sizeof(jsys_stream_settings));
  settings->on_connect = on_client_connect;
  settings->on_data = on_client_data;
  settings->on_end = on_client_end;
  jsys_descriptor *client = jsys_sock_create(loop, AF_INET, SOCK_STREAM);
  client->data = settings;

  r = jsys_tcp_connect(client, 443, "23.212.229.28");
  if (r == -1) return (void *)-1;
  client->callback = tcp_on_client_event;
  r = jsys_loop_add_flags(loop, client, EPOLLOUT);
  if (r == -1) return (void *)-1;
  r = jsys_loop_run(loop);
  if (r == -1) return (void *)-1;
  return 0;
}

int main(int argc, char** argv) {
  void* tret;
  int threads = 1;
  if (argc > 1) {
    threads = atoi(argv[1]);
  }
  SSL_library_init();
  SSL_load_error_strings();
  OpenSSL_add_ssl_algorithms();

  pthread_t pool[threads];
  for (int i = 0; i < threads; i++) {
    pthread_create(&pool[i], NULL, thread_func, NULL);
  }
  for (int i = 0; i < threads; i++) {
    pthread_join(pool[i], &tret);
    fprintf(stderr, "thread %i status: %li\n", i, (long)tret);
  }
  return 0;
}
