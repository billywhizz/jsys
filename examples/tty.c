#include "jsys.h"

#define BUFSIZE 64 * 1024

static char buf[BUFSIZE];
static uint64_t total = 0;
static uint64_t gtotal = 0;

int on_tty_event(jsys_descriptor *client);
int dump_error(jsys_loop* loop, char* message, int r);
int on_timer_event(jsys_descriptor *timer);

int on_tty_event(jsys_descriptor *client) {
  if (jsys_descriptor_is_readable(client)) {
    ssize_t bytes = 0;
    int count = 32;
    int fd = client->fd;
    while ((bytes = read(fd, buf, BUFSIZE))) {
      if (bytes == -1) {
        if (errno == EAGAIN) {
          fprintf(stderr, "count: %i\n", count);
          break;
        }
        perror("read");
        break;
      }
      total += (size_t)bytes;
      gtotal += (size_t)bytes;
      if (--count == 0) break;
    }
  }
  if (jsys_descriptor_is_error(client)) {
    jsys_loop* loop = client->loop;
    jsys_descriptor* timer = client->data;
    jsys_descriptor_free(timer);
    jsys_descriptor_free(client);
    jsys_loop_stop(loop);
    jsys_loop_free(loop);
    fprintf(stderr, "grand total: %lu bytes\n", gtotal);
  }
  return 0;
}

int dump_error(jsys_loop* loop, char* message, int r) {
  fprintf(stderr, "error: %s (%i):\n%s\n", message, r, strerror(errno));
  return r;
}

int on_timer_event(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  fprintf(stderr, "total: %lu Kb\n", (total / (1024)) * 8);
  total = 0;
  return 0;
}

int main() {
  errno = 0;
  int r = 0;
  
  jsys_loop* loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return jsys_dump_error(loop, "loop_init", 0);

  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return dump_error(loop, "timer_create", 0);
  timer->callback = on_timer_event;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return dump_error(loop, "timer_loop_add", r);

  jsys_descriptor *tty = jsys_tty_create(loop, STDIN_FILENO);
  if (tty == NULL) dump_error(loop, "could not open stdin", 1);
  tty->data = timer;
  tty->callback = on_tty_event;
  fprintf(stderr, "fd: %i\n", tty->fd);
  r = jsys_loop_add(loop, tty);
  if (r == -1) return dump_error(loop, "loop_add", r);
  r = jsys_loop_run(loop);
  if (r == -1) return dump_error(loop, "loop_run", r);
  return 0;
}
