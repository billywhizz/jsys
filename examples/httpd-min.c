#include "jsys_http.h"
#include <sys/time.h>

#define BUFSIZE 16384
#define MAXHEADERS 32
#define OUTBUFFERS 512
#define MICROS_PER_SEC 1e6

int requests = 0;
int responses = 0;
double maxrps = 0;
uint64_t totalrequests = 0;
uint64_t totalresponses = 0;
int conn = 0;
uint64_t bytesRead = 0;
uint64_t totalRead = 0;
uint64_t bytesWritten = 0;
uint64_t totalWritten = 0;
uint64_t then = 0;
uint8_t closing = 0;

jsys_loop* loop_descriptor = NULL;
jsys_descriptor* timer_descriptor = NULL;
jsys_descriptor* signal_descriptor = NULL;
jsys_descriptor* server_descriptor = NULL;

int httpd_on_connect(jsys_descriptor *client);
int httpd_on_headers(jsys_descriptor *client);
int httpd_on_body(jsys_descriptor *client, char* chunk, size_t len);
int httpd_on_request(jsys_descriptor *client);
int httpd_on_end(jsys_descriptor *client);
int process_on_timer(jsys_descriptor *timer);
ssize_t process_memory_usage();
int shutdown_handles(jsys_loop*);
int jsys_usage(struct rusage*);
void dump_memory_stats(void);

uint64_t last_user = 0;
uint64_t last_system = 0;

typedef struct memory_stats memory_stats;

struct memory_stats {
  int context_in_alloc;
  int context_in_free;
  size_t context_in_bytes;
  int context_out_alloc;
  int context_out_free;
  size_t context_out_bytes;
  int context_in_base_alloc;
  int context_in_base_free;
  size_t context_in_base_bytes;
  int jsys_httpd_settings_alloc;
  int jsys_httpd_settings_free;
  size_t jsys_httpd_settings_bytes;
  int realloc_in_alloc;
  int realloc_in_free;
  size_t realloc_in_bytes;
  int phr_header_alloc;
  int phr_header_free;
  size_t phr_header_bytes;
  int jsys_http_server_context_alloc;
  int jsys_http_server_context_free;
  size_t jsys_http_server_context_bytes;
  int jsys_http_client_context_alloc;
  int jsys_http_client_context_free;
  size_t jsys_http_client_context_bytes;
  int jsys_stream_settings_alloc;
  int jsys_stream_settings_free;
  size_t jsys_stream_settings_bytes;
  int epoll_event_alloc;
  int epoll_event_free;
  size_t epoll_event_bytes;
  int jsys_stream_context_alloc;
  int jsys_stream_context_free;
  size_t jsys_stream_context_bytes;
  int jsys_descriptor_alloc;
  int jsys_descriptor_free;
  size_t jsys_descriptor_bytes;
  int jsys_loop_alloc;
  int jsys_loop_free;
  size_t jsys_loop_bytes;
  int descriptors_array_alloc;
  int descriptors_array_free;
  size_t descriptors_array_bytes;
  int unknown_alloc;
  int unknown_free;
  size_t unknown_bytes;
};

struct memory_stats stats;

ssize_t process_memory_usage() {
  char buf[1024];
  const char* s = NULL;
  ssize_t n = 0;
  long val = 0;
  int fd = 0;
  int i = 0;
  do {
    fd = open("/proc/self/stat", O_RDONLY);
  } while (fd == -1 && errno == EINTR);
  if (fd == -1) return (ssize_t)errno;
  do
    n = read(fd, buf, sizeof(buf) - 1);
  while (n == -1 && errno == EINTR);
  close(fd);
  if (n == -1)
    return (ssize_t)errno;
  buf[n] = '\0';
  s = strchr(buf, ' ');
  if (s == NULL)
    goto err;
  s += 1;
  if (*s != '(')
    goto err;
  s = strchr(s, ')');
  if (s == NULL)
    goto err;
  for (i = 1; i <= 22; i++) {
    s = strchr(s + 1, ' ');
    if (s == NULL)
      goto err;
  }
  errno = 0;
  val = strtol(s, NULL, 10);
  if (errno != 0)
    goto err;
  if (val < 0)
    goto err;
  return val * getpagesize();
err:
  return 0;
}

void setup_context(jsys_stream_context* context, size_t buffer_size) {
  context->in = (struct iovec*)context->loop->alloc(1, sizeof(struct iovec), "context_in");
  context->out = (struct iovec*)context->loop->alloc(OUTBUFFERS, sizeof(struct iovec), "context_out");
  char* r200 = "HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n";
  int len = strlen(r200);
  int i = 0;
  while (i < OUTBUFFERS) {
    context->out[i].iov_base = (void*)r200;
    context->out[i].iov_len = len;
    i++;
  }
  context->current_buffer = 0;
  context->in->iov_base = context->loop->alloc(1, buffer_size, "context_in_base");
  context->in->iov_len = buffer_size;
}

void free_context(jsys_stream_context* context) {
  jsys_loop* loop = context->loop;
  loop->free(context->in->iov_base, "context_in_base");
  loop->free(context->in, "context_in");
  loop->free(context->out, "context_out");
}

int httpd_on_connect(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  jsys_httpd_settings* http_settings = (jsys_httpd_settings *)context->settings->data;
  setup_context(context, http_settings->buffer_size);
  conn++;
  return 0;
}

int httpd_on_headers(jsys_descriptor *client) {
  return 0;
}

int httpd_on_body(jsys_descriptor *client, char* chunk, size_t len) {
  return 0;
}

int httpd_on_request(jsys_descriptor *client) {
  jsys_stream_context* context = (jsys_stream_context*)client->data;
#if TRACE
  jsys_http_server_context* http = (jsys_http_server_context*)context->data;
  fprintf(stderr, "httpd_on_request (%i), size: (%lu)\n", client->fd, http->header_size);
  fprintf(stderr, "  method   : %.*s\n", (int)http->method_len, http->method);
  fprintf(stderr, "  path     : %.*s\n", (int)http->path_len, http->path);
  fprintf(stderr, "  version  : 1.%i\n", http->minor_version);
  fprintf(stderr, "  body     : %lu\n", http->body_length);
  fprintf(stderr, "  bytes    : %lu\n", http->body_bytes);
  fprintf(stderr, "  headers  :\n");
  size_t i = 0;
  while (i < http->num_headers) {
    fprintf(stderr, "    %.*s : %.*s\n", (int)http->headers[i].name_len, http->headers[i].name, (int)http->headers[i].value_len, http->headers[i].value);
    i++;
  }
#endif
  if (closing == 1) {
    int i = 0;
    char* r200 = "HTTP/1.1 200 OK\r\nContent-Length: 0\r\nConnection: close\r\n\r\n";
    int len = strlen(r200);
    while (i < OUTBUFFERS) {
      context->out[i].iov_base = (void*)r200;
      context->out[i].iov_len = len;
      i++;
    }
  }
  context->current_buffer++;
  if (context->current_buffer == OUTBUFFERS) {
    int r = jsys_tcp_writev(client, context->out, context->current_buffer);
    if (r == -1) return -1;
    context->current_buffer = 0;
  }
  bytesWritten += context->out[0].iov_len;
  requests++;
  responses++;
  return 0;
}

int httpd_on_data(jsys_descriptor *client, size_t len) {
  bytesRead += len;
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  if (context->current_buffer == 0) return;
  int r = 0;
  if (context->current_buffer == 1) {
    r = jsys_tcp_write(client, &context->out[context->current_buffer]);
  } else {
    r = jsys_tcp_writev(client, context->out, context->current_buffer);
  }
  context->current_buffer = 0;
  return 0;
}

int httpd_on_end(jsys_descriptor *client) {
  free_context((jsys_stream_context*)client->data);
  conn--;
  return 0;
}

static uint64_t time_us() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return (t.tv_sec * 1000000) + t.tv_usec;
}

int process_on_timer(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  uint64_t now = time_us();
  uint64_t diff = now - then;
  then = now;
  double rps = (double)requests / ((double)diff / 1000000);
  double rsps = (double)responses / ((double)diff / 1000000);
  ssize_t mem = process_memory_usage();
  jsys_loop* loop = timer->loop;
  totalRead += bytesRead;
  totalWritten += bytesWritten;
  totalrequests += requests;
  totalresponses += responses;
  int ct_closing = 0;
  int ct_closed = 0;
  int ct_socket = 0;
  int ct_tty = 0;
  int ct_signal = 0;
  int ct_timer = 0;
  int ct_unknown = 0;
  for (int i = 0; i < loop->maxfds; i++) {
    jsys_descriptor* handle = loop->descriptors[i];
    if (handle != NULL) {
      ct_closing += handle->closing;
      ct_closed += handle->closed;
      switch(handle->type) {
        case JSYS_SOCKET:
          ct_socket++;
          break;
        case JSYS_TTY:
          ct_tty++;
          break;
        case JSYS_SIGNAL:
          ct_signal++;
          break;
        case JSYS_TIMER:
          ct_timer++;
          break;
        default:
          break;
      }
    }
  }
  struct rusage usage;
  jsys_usage(&usage);
  uint64_t user = (MICROS_PER_SEC * usage.ru_utime.tv_sec) + usage.ru_utime.tv_usec;
  uint64_t system = (MICROS_PER_SEC * usage.ru_stime.tv_sec) + usage.ru_stime.tv_usec;
  uint64_t user_elapsed = user - last_user;
  uint64_t system_elapsed = system - last_system;
  double total_usage = (user_elapsed + system_elapsed) / 1000000.0;
  double user_usage = user_elapsed / 1000000.0;
  double system_usage = system_elapsed / 1000000.0;
  fprintf(stdout, "\33[35;0H");
  fprintf(stdout, "%c[1J", 0x1B);
  fprintf(stdout, "%c[H", 0x1B);
  fprintf(stdout, "\
\x1B[36mpid                      \x1B[0m %5i\n\
\x1B[36mgeneral                   \x1B[36mmem \x1B[0m %10lu \x1B[36m  conn    \x1B[0m %7i\n\
\x1B[36mrate                     \x1B[0m \x1B[36mreq\x1B[0m %11.2f   \x1B[36mres \x1B[0m %11.2f ( \x1B[31m%11.2f\x1B[0m )\n\
\x1B[36m                         \x1B[0m \x1B[36mreq \x1B[0m %10lu   \x1B[36mres  \x1B[0m %10lu \n\
\x1B[36mnetwork                  \x1B[0m \x1B[36mread\x1B[0m %10lu   \x1B[36mwrite\x1B[0m %10lu \n\
\x1B[36m                         \x1B[0m \x1B[36mread\x1B[0m %10lu   \x1B[36mwrite\x1B[0m %10lu \n\
\x1B[36mload                     \x1B[0m \x1B[32muser      \x1B[0m %2.2f   \x1B[31msystem     \x1B[0m %2.2f   ( \x1B[33m%2.2f\x1B[0m ) \n\
\x1B[36mhandles                   \x1B[35msck\x1B[0m %11i\n\
                          \x1B[35mtty\x1B[0m %11i\n\
                          \x1B[35msig\x1B[0m %11i\n\
                          \x1B[35mtim\x1B[0m %11i\n\
                          \x1B[35munk\x1B[0m %11i\n\
                          \x1B[35mclo\x1B[0m %11i\n\
                          \x1B[35mfin\x1B[0m %11i\n\
", getpid(), mem, conn, rps, rsps, maxrps,totalrequests, totalresponses, bytesRead, bytesWritten, totalRead, totalWritten, user_usage, system_usage, total_usage, ct_socket, ct_tty, ct_signal, ct_timer, ct_unknown, ct_closed, ct_closing);
  last_user = user;
  last_system = system;
  bytesRead = 0;
  bytesWritten = 0;
  if (requests > maxrps) maxrps = requests;
  requests = 0;
  responses = 0;
  dump_memory_stats();
  return 0;
}

int shutdown_handles(jsys_loop* loop) {
  int r = jsys_descriptor_free(signal_descriptor);
  if (r == -1) return jsys_dump_error(loop, "free_signal", r);
  r = jsys_http_free_server(server_descriptor);
  if (r == -1) return jsys_dump_error(loop, "free_server", r);
  r = jsys_descriptor_free(timer_descriptor);
  if (r == -1) return jsys_dump_error(loop, "free_timer", r);
  return 0;
}

int process_on_signal(jsys_descriptor* signal) {
	struct signalfd_siginfo info;
	ssize_t r = read(signal->fd, &info, sizeof info);
  jsys_loop* loop = signal->loop;
	if (r != sizeof info) return -1;
	fprintf(stderr, "on_signal %s (%u)\n", strsignal((int)info.ssi_signo), info.ssi_signo);
	fprintf(stderr, "  signo   = %u\n", info.ssi_signo);
	fprintf(stderr, "  errno   = %i\n", info.ssi_errno);
	fprintf(stderr, "  code    = %i\n", info.ssi_code);
	fprintf(stderr, "  pid     = %u\n", info.ssi_pid);
	fprintf(stderr, "  uid     = %u\n", info.ssi_uid);
	fprintf(stderr, "  fd      = %i\n", info.ssi_fd);
	fprintf(stderr, "  tid     = %u\n", info.ssi_tid);
	fprintf(stderr, "  band    = %u\n", info.ssi_band);
	fprintf(stderr, "  overrun = %u\n", info.ssi_overrun);
	fprintf(stderr, "  trapno  = %u\n", info.ssi_trapno);
	fprintf(stderr, "  status  = %i\n", info.ssi_status);
	fprintf(stderr, "  int     = %i\n", info.ssi_int);
	fprintf(stderr, "  ptr     = %lu\n", info.ssi_ptr);
	fprintf(stderr, "  utime   = %lu\n", info.ssi_utime);
	fprintf(stderr, "  stime   = %lu\n", info.ssi_stime);
	fprintf(stderr, "  addr    = %lu\n", info.ssi_addr);
  r = shutdown_handles(loop);
  if (r == -1) return jsys_dump_error(loop, "shutdown_handles", r);
  closing = 1;
  // loop should be empty once connections close
  return 0;
}

void* myalloc(size_t elements, size_t size, const char* tag) {
  //fprintf(stderr, "%s : alloc %lu elements of %lu\n", tag, elements, size);
  int taglen = strlen(tag);
  if (strncasecmp(tag, "context_in", taglen) == 0) {
    stats.context_in_alloc += 1;
    stats.context_in_bytes += size;
  } else if (strncasecmp(tag, "context_out", taglen) == 0) {
    stats.context_out_alloc += 1;
    stats.context_out_bytes += size;
  } else if (strncasecmp(tag, "context_in_base", taglen) == 0) {
    stats.context_in_base_alloc += 1;
    stats.context_in_base_bytes += size;
  } else if (strncasecmp(tag, "jsys_httpd_settings", taglen) == 0) {
    stats.jsys_httpd_settings_alloc += 1;
    stats.jsys_httpd_settings_bytes += size;
  } else if (strncasecmp(tag, "realloc_in", taglen) == 0) {
    stats.realloc_in_alloc += 1;
    stats.realloc_in_bytes += size;
  } else if (strncasecmp(tag, "phr_header", taglen) == 0) {
    stats.phr_header_alloc += 1;
    stats.phr_header_bytes += size;
  } else if (strncasecmp(tag, "jsys_http_server_context", taglen) == 0) {
    stats.jsys_http_server_context_alloc += 1;
    stats.jsys_http_server_context_bytes += size;
  } else if (strncasecmp(tag, "jsys_http_client_context", taglen) == 0) {
    stats.jsys_http_client_context_alloc += 1;
    stats.jsys_http_client_context_bytes += size;
  } else if (strncasecmp(tag, "jsys_stream_settings", taglen) == 0) {
    stats.jsys_stream_settings_alloc += 1;
    stats.jsys_stream_settings_bytes += size;
  } else if (strncasecmp(tag, "jsys_descriptor", taglen) == 0) {
    stats.jsys_descriptor_alloc += 1;
    stats.jsys_descriptor_bytes += size;
  } else if (strncasecmp(tag, "epoll_event", taglen) == 0) {
    stats.epoll_event_alloc += 1;
    stats.epoll_event_bytes += size;
  } else if (strncasecmp(tag, "jsys_stream_context", taglen) == 0) {
    stats.jsys_stream_context_alloc += 1;
    stats.jsys_stream_context_bytes += size;
  } else if (strncasecmp(tag, "jsys_loop", taglen) == 0) {
    stats.jsys_loop_alloc += 1;
    stats.jsys_loop_bytes += size;
  } else if (strncasecmp(tag, "descriptors_array", taglen) == 0) {
    stats.descriptors_array_alloc += 1;
    stats.descriptors_array_bytes += size;
  } else {
    fprintf(stderr, "unknown alloc: %s\n", tag);
    stats.unknown_alloc += 1;
    stats.unknown_bytes += size;
  }
  return calloc(elements, size);
}

void myfree(void* ptr, const char* tag) {
  //fprintf(stderr, "%s : free\n", tag);
  int taglen = strlen(tag);
  if (strncasecmp(tag, "context_in", taglen) == 0) {
    stats.context_in_free += 1;
  } else if (strncasecmp(tag, "context_out", taglen) == 0) {
    stats.context_out_free += 1;
  } else if (strncasecmp(tag, "context_in_base", taglen) == 0) {
    stats.context_in_base_free += 1;
  } else if (strncasecmp(tag, "jsys_httpd_settings", taglen) == 0) {
    stats.jsys_httpd_settings_free += 1;
  } else if (strncasecmp(tag, "realloc_in", taglen) == 0) {
    stats.realloc_in_free += 1;
  } else if (strncasecmp(tag, "phr_header", taglen) == 0) {
    stats.phr_header_free += 1;
  } else if (strncasecmp(tag, "jsys_http_server_context", taglen) == 0) {
    stats.jsys_http_server_context_free += 1;
  } else if (strncasecmp(tag, "jsys_http_client_context", taglen) == 0) {
    stats.jsys_http_client_context_free += 1;
  } else if (strncasecmp(tag, "jsys_stream_settings", taglen) == 0) {
    stats.jsys_stream_settings_free += 1;
  } else if (strncasecmp(tag, "jsys_descriptor", taglen) == 0) {
    stats.jsys_descriptor_free += 1;
  } else if (strncasecmp(tag, "epoll_event", taglen) == 0) {
    stats.epoll_event_free += 1;
  } else if (strncasecmp(tag, "jsys_stream_context", taglen) == 0) {
    stats.jsys_stream_context_free += 1;
  } else if (strncasecmp(tag, "jsys_loop", taglen) == 0) {
    stats.jsys_loop_free += 1;
  } else if (strncasecmp(tag, "descriptors_array", taglen) == 0) {
    stats.descriptors_array_free += 1;
  } else {
    fprintf(stderr, "unknown free: %s\n", tag);
    stats.unknown_free += 1;
  }
  free(ptr);
  return;
}

void dump_memory_stats() {
  fprintf(stdout, "\
\x1B[36mcontext_in               \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mcontext_out              \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mcontext_in_base          \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mjsys_httpd_settings      \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mrealloc_in               \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mphr_header               \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mjsys_http_server_context \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mjsys_http_client_context \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mjsys_stream_settings     \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mjsys_descriptor          \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mepoll_event              \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mjsys_stream_context      \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mjsys_loop                \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36mdescriptors_array        \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\x1B[36munknown                  \x1B[0m \x1B[36malloc\x1B[0m %5u     \x1B[36mfree\x1B[0m %5u \x1B[36mbytes\x1B[0m %12lu \n\
\n",stats.context_in_alloc, stats.context_in_free, stats.context_in_bytes,
stats.context_out_alloc, stats.context_out_free, stats.context_out_bytes,
stats.context_in_base_alloc, stats.context_in_base_free, stats.context_in_base_bytes,
stats.jsys_httpd_settings_alloc, stats.jsys_httpd_settings_free, stats.jsys_httpd_settings_bytes,
stats.realloc_in_alloc, stats.realloc_in_free, stats.realloc_in_bytes,
stats.phr_header_alloc, stats.phr_header_free, stats.phr_header_bytes,
stats.jsys_http_server_context_alloc, stats.jsys_http_server_context_free, stats.jsys_http_server_context_bytes,
stats.jsys_http_client_context_alloc, stats.jsys_http_client_context_free, stats.jsys_http_client_context_bytes,
stats.jsys_stream_settings_alloc, stats.jsys_stream_settings_free, stats.jsys_stream_settings_bytes,
stats.jsys_descriptor_alloc, stats.jsys_descriptor_free, stats.jsys_descriptor_bytes,
stats.epoll_event_alloc, stats.epoll_event_free, stats.epoll_event_bytes,
stats.jsys_stream_context_alloc, stats.jsys_stream_context_free, stats.jsys_stream_context_bytes,
stats.jsys_loop_alloc, stats.jsys_loop_free, stats.jsys_loop_bytes,
stats.descriptors_array_alloc, stats.descriptors_array_free, stats.descriptors_array_bytes,
stats.unknown_alloc, stats.unknown_free, stats.unknown_bytes);
}

int main(int argc, char** argv) {
  int r = 0;

  jsys_loop* loop = jsys_loop_create_with_allocator(EPOLL_CLOEXEC, 128, 1024, myfree, myalloc);
  if (loop == NULL) return 1;
  jsys_descriptor* timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return 1;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return jsys_dump_error(loop, "loop_add timer", r);
  timer->callback = process_on_timer;
  signal(SIGWINCH, SIG_IGN);

  jsys_signal_add(loop, SIGTERM);
  jsys_descriptor* signal = jsys_signal_watcher_create(loop);
  if (signal == NULL) return 1;
  r = jsys_loop_add_flags(loop, signal, EPOLLIN); // level triggered
  if (r == -1) return 1;
  signal->callback = process_on_signal;

  jsys_httpd_settings* http_settings = jsys_http_create_httpd_settings(loop);
  http_settings->on_headers = httpd_on_headers;
  http_settings->on_request = httpd_on_request;
  http_settings->on_body = httpd_on_body;
  http_settings->on_connect = httpd_on_connect;
  http_settings->on_end = httpd_on_end;
  http_settings->on_data = httpd_on_data;
  http_settings->buffer_size = BUFSIZE;
  http_settings->max_headers = MAXHEADERS;
  http_settings->domain = AF_INET;
  http_settings->type = SOCK_STREAM;
  http_settings->buffers = OUTBUFFERS;

  jsys_descriptor* server = jsys_http_create_server(loop, http_settings);
  r = jsys_tcp_bind_reuse(server, 3000, INADDR_ANY);
  if (r == -1) return jsys_dump_error(loop, "tcp_bind", r);
  r = jsys_tcp_listen(server, SOMAXCONN);
  if (r == -1) return jsys_dump_error(loop, "tcp_listen", r);
  r = jsys_loop_add(loop, server);
  if (r == -1) return jsys_dump_error(loop, "loop_add server", r);

  then = time_us();

  loop_descriptor = loop;
  timer_descriptor = timer;
  signal_descriptor = signal;
  server_descriptor = server;

  r = jsys_loop_run(loop);
  if (r == -1) return jsys_dump_error(loop, "loop_run", r);
  jsys_loop_free(loop);
  dump_memory_stats();

  return 0;
}
