#include "jsys.h"
#include <pthread.h>

#define BUFSIZE 65536

static jsys_loop *loop;
__thread int conn;
__thread int rps;

int on_client_data(jsys_descriptor *client, size_t bytes);
int on_timer_event(jsys_descriptor *timer);
int on_client_connect(jsys_descriptor* client);
int on_client_end(jsys_descriptor* client);
int on_signal(jsys_descriptor* signal);

int on_client_data(jsys_descriptor *client, size_t bytes) {
  //jsys_stream_context* context = (jsys_stream_context*)client->data;
  //fprintf(stderr, "client_data: (%i) %lu\n%.*s\n", client->fd, bytes, (int)bytes, (char*)context->in->iov_base);
  rps++;
  return jsys_tcp_shutdown(client);
}

int on_timer_event(jsys_descriptor *timer) {
  jsys_timer_read(timer);
  jsys_loop* loop = timer->loop;
  fprintf(stderr, "connections: %i, rps: %i\n", conn, rps);
  for (int i = 0; i < loop->maxfds; i++) {
    if (loop->descriptors[i] != NULL) {
      //fprintf(stderr, "  descriptor (%s) %i\n", jsys_descriptor_name(loop->descriptors[i]), loop->descriptors[i]->fd);
    }
  }
  rps = 0;
  return 0;
}

int on_client_connect(jsys_descriptor* client) {
  int r = jsys_loop_mod_flags(client, EPOLLIN | EPOLLET);
  if (r == -1) return r;
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  context->in = calloc(1, sizeof(struct iovec));
  context->out = calloc(1, sizeof(struct iovec));
  context->in->iov_base = calloc(1, BUFSIZE);
  context->in->iov_len = BUFSIZE;
  context->out->iov_base = "GET / HTTP/1.1\r\nHost: foo\r\n\r\n";
  context->out->iov_len = strlen(context->out->iov_base);
  conn++;
  return jsys_tcp_write(client, context->out);
}

int new_client(jsys_loop* loop, jsys_stream_settings* settings) {
  jsys_descriptor *client = jsys_sock_create(loop, AF_INET, SOCK_STREAM);
  client->data = settings;
  int r = jsys_tcp_connect(client, 3000, "127.0.0.1");
  if (r == -1) return r;
  r = jsys_loop_add_flags(loop, client, EPOLLOUT);
  if (r == -1) return r;
  return 0;
}

int on_client_end(jsys_descriptor* client) {
  conn--;
  jsys_loop* loop = client->loop;
  jsys_stream_context* context = (jsys_stream_context*)client->data;
  jsys_stream_settings* settings = context->data;
  free(context->in);
  free(context->out);
  free(context);
  return new_client(loop, settings);
}

int on_signal(jsys_descriptor* signal) {
	struct signalfd_siginfo info;
	ssize_t r = read(signal->fd, &info, sizeof info);
	if (r != sizeof info) return -1;
	fprintf(stderr, "on_signal %s (%u)\n", strsignal((int)info.ssi_signo), info.ssi_signo);
	fprintf(stderr, "  signo   = %u\n", info.ssi_signo);
	fprintf(stderr, "  errno   = %i\n", info.ssi_errno);
	fprintf(stderr, "  code    = %i\n", info.ssi_code);
	fprintf(stderr, "  pid     = %u\n", info.ssi_pid);
	fprintf(stderr, "  uid     = %u\n", info.ssi_uid);
	fprintf(stderr, "  fd      = %i\n", info.ssi_fd);
	fprintf(stderr, "  tid     = %u\n", info.ssi_tid);
	fprintf(stderr, "  band    = %u\n", info.ssi_band);
	fprintf(stderr, "  overrun = %u\n", info.ssi_overrun);
	fprintf(stderr, "  trapno  = %u\n", info.ssi_trapno);
	fprintf(stderr, "  status  = %i\n", info.ssi_status);
	fprintf(stderr, "  int     = %i\n", info.ssi_int);
	fprintf(stderr, "  ptr     = %lu\n", info.ssi_ptr);
	fprintf(stderr, "  utime   = %lu\n", info.ssi_utime);
	fprintf(stderr, "  stime   = %lu\n", info.ssi_stime);
	fprintf(stderr, "  addr    = %lu\n", info.ssi_addr);
  return 0;
}

void* thread_func(void* arg) {
  int r = 0;
  loop = jsys_loop_create(EPOLL_CLOEXEC, 128, 1024);
  if (loop == NULL) return (void *)-1;

  jsys_signal_add(loop, SIGPIPE);
  jsys_descriptor *signal = jsys_signal_watcher_create(loop);
  if (signal == NULL) return (void *)-1;
  r = jsys_loop_add_flags(loop, signal, EPOLLIN); // level triggered
  if (r == -1) return (void *)-1;
  signal->callback = on_signal;

  jsys_descriptor *timer = jsys_timer_create(loop, 1000000000, 1000000000);
  if (timer == NULL) return (void *)-1;
  r = jsys_loop_add(loop, timer);
  if (r == -1) return (void *)-1;
  timer->callback = on_timer_event;

  jsys_stream_settings* settings = calloc(1, sizeof(jsys_stream_settings));
  settings->on_connect = on_client_connect;
  settings->on_data = on_client_data;
  settings->on_end = on_client_end;

  for (int i = 0; i < 1024; i++) {
    if (new_client(loop, settings) != 0) break;
  }
  r = jsys_loop_run(loop);
  if (r == -1) return (void *)-1;
  return 0;
}

int main(int argc, char** argv) {
  void* tret;
  int threads = 1;
  if (argc > 1) {
    threads = atoi(argv[1]);
  }
  pthread_t pool[threads];
  for (int i = 0; i < threads; i++) {
    pthread_create(&pool[i], NULL, thread_func, NULL);
  }
  for (int i = 0; i < threads; i++) {
    pthread_join(pool[i], &tret);
    fprintf(stderr, "thread %i status: %li\n", i, (long)tret);
  }
  return 0;
}
